# Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#
#     * Neither the name of the University of California, Santa Barbara nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

IF (COMMAND cmake_policy)
  CMAKE_POLICY(SET CMP0003 NEW)
ENDIF (COMMAND cmake_policy)

PROJECT(EVSAC C CXX)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR})

SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

# Default locations to search for on various platforms.
LIST(APPEND SEARCH_LIBS /usr/lib)
LIST(APPEND SEARCH_LIBS /usr/local/lib)
LIST(APPEND SEARCH_LIBS /usr/local/homebrew/lib) # Mac OS X
LIST(APPEND SEARCH_LIBS /opt/local/lib) # Mac OS X (macports)

LIST(APPEND SEARCH_HEADERS /usr/include)
LIST(APPEND SEARCH_HEADERS /usr/local/include)
LIST(APPEND SEARCH_HEADERS /usr/local/homebrew/include) # Mac OS X
LIST(APPEND SEARCH_HEADERS /opt/local/include) # Mac OS X (macports)

# Compiler-specific C++11 activation.
if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU")
    execute_process(
        COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
    if (NOT (GCC_VERSION VERSION_GREATER 4.7 OR GCC_VERSION VERSION_EQUAL 4.7))
        message(FATAL_ERROR "${PROJECT_NAME} requires g++ 4.7 or greater.")
    endif ()
    set(CMAKE_CXX_FLAGS "-std=c++11")
elseif ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    set(CMAKE_CXX_FLAGS "-std=c++11 -stdlib=libc++")
else ()
    message(FATAL_ERROR "Your C++ compiler does not support C++11.")
endif ()

# Do I really need Eigen?
# Eigen
FIND_PACKAGE(Eigen REQUIRED)
IF (EIGEN_FOUND)
  MESSAGE("-- Found Eigen version ${EIGEN_VERSION}: ${EIGEN_INCLUDE_DIRS}")
ENDIF (EIGEN_FOUND)
INCLUDE_DIRECTORIES(${EIGEN_INCLUDE_DIRS})

# Google Flags
FIND_PACKAGE(Gflags REQUIRED)
IF(GFLAGS_FOUND)
INCLUDE_DIRECTORIES(${GFLAGS_INCLUDE_DIRS})
ENDIF(GFLAGS_FOUND)

# Google Logging
FIND_PACKAGE(Glog REQUIRED)
IF(GLOG_FOUND)
INCLUDE_DIRECTORIES(${GLOG_INCLUDE_DIRS})
ENDIF(GLOG_FOUND)

# Google ceres-solver
IF(WITH_CERES)
  MESSAGE("-- Check for Google ceres-solver")
  FIND_LIBRARY(CERES_LIB NAMES ceres PATHS ${SEARCH_LIBS})
  IF (NOT EXISTS ${CERES_LIB})
    MESSAGE(FATAL_ERROR
      "Can't find Google ceres-solver. Please specify: "
      "-DCERES_LIB=...")
  ENDIF (NOT EXISTS ${CERES_LIB})
  MESSAGE("-- Found Google ceres-solver library: ${CERES_LIB}")

  # BLAS
  FIND_PACKAGE(BLAS REQUIRED)

  # LAPACK
  FIND_PACKAGE(LAPACK REQUIRED)
ENDIF(WITH_CERES)

# STATX
MESSAGE("-- Check for libstatx")
FIND_PATH(STATX_INCLUDE_DIR NAMES statx/distributions/gamma.h PATHS ${SEARCH_HEADERS})
IF (NOT EXISTS ${STATX_INCLUDE_DIR})
  MESSAGE(FATAL_ERROR "Can't find statx headers. Try passing -DSTATX_INCLUDE_DIR=...")
ENDIF (NOT EXISTS ${STATX_INCLUDE_DIR})
MESSAGE("-- Found statx headers: ${STATX_INCLUDE_DIR}")

FIND_LIBRARY(STATX_LIB NAMES statx PATHS ${SEARCH_LIBS})
IF (NOT EXISTS ${STATX_LIB})
  MESSAGE(FATAL_ERROR "Can't find statx. Please specify: -DSTATX_LIB=...")
ENDIF(NOT EXISTS ${STATX_LIB})
MESSAGE("-- Found statx library: ${STATX_LIB}")

# OpenCV
FIND_PACKAGE(OpenCV REQUIRED)
IF (NOT ${OpenCV_FOUND})
  MESSAGE(FATAL_ERROR "OpenCV Libraries were not found!")
ENDIF(NOT ${OpenCV_FOUND})
INCLUDE_DIRECTORIES(${OpenCV_INCLUDE_DIRS})
MESSAGE("-- OpenCV libraries: ${OpenCV_LIBRARIES}")

# Include optimo headers
# TODO(vfragoso): Create a FindOptimo.cmake 
MESSAGE("-- Check for optimo")
FIND_PATH(OPTIMO_INCLUDE_DIR NAMES optimo/solver/solver.h PATHS ${SEARCH_HEADERS})
IF (NOT EXISTS ${OPTIMO_INCLUDE_DIR})
  MESSAGE(FATAL_ERROR "Can't find optimo headers. Try passing -DOPTIMO_INCLUDE_DIR=...")
ENDIF (NOT EXISTS ${OPTIMO_INCLUDE_DIR})
MESSAGE("-- Found optimo headers: ${OPTIMO_INCLUDE_DIR}")
INCLUDE_DIRECTORIES(${OPTIMO_INCLUDE_DIR})

# Setting CXX FLAGS appropriately. The code below was inspired from
# Google CERES and modified for this library.
SET (CMAKE_BUILD_TYPE Release)
SET (STATX_CXX_FLAGS)

IF (CMAKE_BUILD_TYPE STREQUAL "Release")
  IF (CMAKE_COMPILER_IS_GNUCXX)
    # Linux
    IF (CMAKE_SYSTEM_NAME MATCHES "Linux")
      IF (NOT GCC_VERSION VERSION_LESS 4.2)
        SET (STATX_CXX_FLAGS 
          "${STATX_CXX_FLAGS} -march=native -mtune=native -msse2 -msse3 -msse4")
      ENDIF (NOT GCC_VERSION VERSION_LESS 4.2)
    ENDIF (CMAKE_SYSTEM_NAME MATCHES "Linux")
    # Mac OS X
    IF (CMAKE_SYSTEM_NAME MATCHES "Darwin")
      SET (STATX_CXX_FLAGS "${STATX_CXX_FLAGS} -msse3 -msse4 -msse3 -msse2")
      EXECUTE_PROCESS(COMMAND ${CMAKE_C_COMPILER}
        ARGS ${CMAKE_CXX_COMPILER_ARG1} -dumpversion
        OUTPUT_VARIABLE GCC_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE)
      IF (GCC_VERSION VERSION_LESS 4.3)
        SET (STATX_CXX_FLAGS "${STATX_CXX_FLAGS} -fast")
      ENDIF (GCC_VERSION VERSION_LESS 4.3)
    ENDIF (CMAKE_SYSTEM_NAME MATCHES "Darwin")
  ENDIF (CMAKE_COMPILER_IS_GNUCXX)
  IF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    SET (STATX_CXX_FLAGS "-Ofast -ffast-math -fvectorize -funroll-loops")
  ENDIF ()
ENDIF (CMAKE_BUILD_TYPE STREQUAL "Release")

SET (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${STATX_CXX_FLAGS}")

IF (CMAKE_BUILD_TYPE STREQUAL "Debug")
  IF (CMAKE_COMPILER_IS_GNUCXX)
    # Linux
    IF (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
      SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g3 -O0")
    ENDIF (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    # Mac OS X
    IF (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
      SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g3 -O0")
    ENDIF (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  ENDIF (CMAKE_COMPILER_IS_GNUCXX)
  # CLANG
  IF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS_DEBUG} -g3 -O0")
  ENDIF ()
ENDIF (CMAKE_BUILD_TYPE STREQUAL "Debug")

# Executables 
ADD_EXECUTABLE(evsac_example evsac.cc evsac_example.cc)
TARGET_LINK_LIBRARIES(evsac_example ${STATX_LIB} ${GFLAGS_LIBRARIES} ${GLOG_LIBRARIES} ${OpenCV_LIBRARIES})