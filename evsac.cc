// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "evsac.h"

#include <utility>
#include <vector>

#include "glog/logging.h"
#include "optimo/solvers/primal_dual_qp.h"
#include "statx/distributions/gamma.h"
#include "statx/distributions/evd/gev.h"
#include "statx/utils/ecdf.h"

#include "mrrayleigh.h"
#include "homo.h"
#include "fundmat.h"
#include "ransac.h"

void EVSAC::ComputeHomography(
    const std::vector<std::vector<cv::DMatch> >& match_results,
    const std::vector<cv::Point2f>& ref_kpts,
    const std::vector<cv::Point2f>& qry_kpts,
    EVSAC::MixtureModelParams* params,
    Eigen::MatrixXd* h) {
  if (!h || !params) {
    LOG(INFO) << "Invalid output argument";
    return;
  }

  // Calculate mixture model.
  std::vector<float> prob;
  CalculateMixtureModel(match_results, params, &prob);

  // Build correspondences according to match_results.
  std::vector<cv::Point2f> ref_pts(qry_kpts.size());
  std::vector<cv::Point2f> qry_pts(qry_kpts.size());

  for (int i = 0; i < qry_kpts.size(); ++i) {
    const std::vector<cv::DMatch>& row = match_results[i];
    const cv::DMatch& nn = row[0];
    ref_pts[i] = ref_kpts[nn.trainIdx];
    qry_pts[i] = qry_kpts[nn.queryIdx];
  }

  // Estimate homography.
  RandomSampler evsac_sampler(prob);
  Ransac ransac(0.90, 5.0, 0.9*params->inlier_ratio);
  Homography homography;
  std::vector<bool> inliers;
  LOG(INFO) << "Estimating homography...";
  bool exit_flag = ransac.Estimate(homography, evsac_sampler, ref_pts,
                                   qry_pts, h, &inliers);
  LOG_IF(INFO, !exit_flag) << "Ransac did not converge";
}

void EVSAC::ComputeFundamentalMatrix(
    const std::vector<std::vector<cv::DMatch> >& match_results,
    const std::vector<cv::Point2f>& ref_kpts,
    const std::vector<cv::Point2f>& qry_kpts,
    EVSAC::MixtureModelParams* params,
    Eigen::MatrixXd* f) {
  if (!f || !params) {
    LOG(INFO) << "Invalid output argument";
    return;
  }

  // Calculate mixture model.
  std::vector<float> prob;
  CalculateMixtureModel(match_results, params, &prob);

  // Build correspondences according to match_results.
  std::vector<cv::Point2f> ref_pts(qry_kpts.size());
  std::vector<cv::Point2f> qry_pts(qry_kpts.size());

  for (int i = 0; i < qry_kpts.size(); ++i) {
    const std::vector<cv::DMatch>& row = match_results[i];
    const cv::DMatch& nn = row[0];
    ref_pts[i] = ref_kpts[nn.trainIdx];
    qry_pts[i] = qry_kpts[nn.queryIdx];
  }

  // Estimate homography.
  RandomSampler evsac_sampler(prob);
  Ransac ransac(0.90, 1e-3, 0.9*params->inlier_ratio);
  // Implementation of 8pt algorithm.
  FundamentalMatrix fmat_estimator;
  std::vector<bool> inliers;
  LOG(INFO) << "Estimating fundamental matrix...";
  bool exit_flag = ransac.Estimate(fmat_estimator, evsac_sampler, ref_pts,
                                   qry_pts, f, &inliers);
  LOG_IF(INFO, !exit_flag) << "Ransac did not converge";
}

void
EVSAC::CalculateMixtureModel(
    const std::vector<std::vector<cv::DMatch> >& match_results,
    EVSAC::MixtureModelParams* params,
    std::vector<float>* prob) {
  using optimo::solvers::PrimalDualQP;
  using statx::FitGamma;
  using statx::FitGEV;
  using statx::GammaCdf;
  using statx::GEVCdf;
  using statx::GammaPdf;
  using statx::GEVPdf;
  // Predict!
  // For every query compute a prediction.
  const int nref_kpts = match_results[0].size();
  const int nqry_kpts = match_results.size();
  std::vector<float> distances(nref_kpts);
  std::vector<double> first_distances;
  // For fitting GEV.
  std::vector<double> second_distances;
  // For fitting Gamma.
  std::vector<double> pred_inlier_distances;
  // prediction results.
  std::vector<bool> prediction_results(nqry_kpts);
  LOG(INFO) << "Predicting...";
  double tao = 0.0;
  for (int k = 0; k < nqry_kpts; k++) {
    const std::vector<cv::DMatch>& row = match_results[k];
    // Store the -distance as GEV is expecting data as maxima.
    second_distances.push_back(-row[1].distance);
    for (int i = 0; i < nref_kpts; i++) {
      // Build the distance vector.
      const cv::DMatch& dmatch = row[i];
      distances[i] = dmatch.distance;
    }

    // Predict!
    prediction_results[k] = mrrayleigh(distances, p_, eps_);
    if (prediction_results[k]) {
      pred_inlier_distances.push_back(distances[0]);
      tao += 1.0;
    }
    first_distances.push_back(distances[0]);
  }

  // Fit distributions.
  const bool gam_flag =
      FitGamma(pred_inlier_distances, &params->k, &params->theta);
  LOG(INFO) << "Gamma distribution: k=" << params->k
            << " theta=" << params->theta
            << " flag: " << gam_flag;

  // Fit GEV.
  statx::FitType dist_fit_type =
      (fit_method_ == MLE) ? statx::MLE : statx::QUANTILE_NLS;
  const bool gev_flag =
      FitGEV(second_distances, dist_fit_type,
             &params->mu, &params->sigma, &params->xi);
  LOG(INFO) << "GEV distribution: mu=" << params->mu
            << " sigma=" << params->sigma
            << " xi=" << params->xi << " flag: " << gev_flag;

  // Calculate Inlier ratio by solving the proposed LS problem
  // min_x 0.5 * norm(y - Ax)^2
  // subject to
  // [1 1] x = 1
  // [0 0]' <= x <= [tao 1]'
  // where x = [inlier_ratio (1-inlier_ratio)]
  // This LS problem can be transformed into a QP problem
  // min_x 0.5*x'A'*A*x - 2*y'*A*x + y'*y
  // subject to
  // [1 1] x = 1
  // [0 0]' <= x <= [tao 1]'
  PrimalDualQP<double, 2, 4, 1> qp_solver;
  qp_solver.options.max_iter_ = 100;
  PrimalDualQP<double, 2, 4, 1>::Params qp_params;

  // Tao.
  tao /= match_results.size();
  LOG(INFO) << "tao=" << tao;

  // Inequality Constraints.
  auto& Ain = qp_params.Ain;
  Ain.setConstant(0.0);
  // For inequality x > 0.
  Ain(0, 0) = -1;
  Ain(1, 1) = -1;
  // For inequality x < [tao 1]'.
  Ain(2, 0) = 1;
  Ain(3, 1) = 1;
  VLOG(1) << "Ain: \n" << Ain;

  auto& bin = qp_params.bin;
  bin.setConstant(0.0);
  // inlier ratio from prediction.
  bin(2) = tao;
  bin(3) = 1.0;
  VLOG(1) << "bin: \n" << bin;

  // Equality Constraints.
  auto& Aeq = qp_params.Aeq;
  Aeq.setConstant(1.0);
  VLOG(1) << "Aeq: \n" << Aeq;

  auto& beq = qp_params.beq;
  beq(0, 0) = 1.0;
  VLOG(1) << "beq: \n" << beq;

  // fx = f_cdf(x).
  std::vector<double> fx, x;
  statx::utils::EmpiricalCdf(first_distances, &fx, &x);
  Eigen::Map<Eigen::VectorXd> y(&fx[0], fx.size());

  // Calculate matrix A.
  const int nx = x.size();
  Eigen::MatrixXd A(nx, 2);

  // Calculate matrix and evaluate the pdfs for calculating posterior in the
  // same loop.
  for (int i = 0; i < nx; i++) {
    A(i, 0) = GammaCdf(x[i], params->k, params->theta);
    A(i, 1) = 1.0 - GEVCdf(-x[i], params->mu, params->sigma, params->xi);
  }
  qp_params.Q = A.transpose()*A;
  qp_params.d = -1.0*A.transpose()*y;

  VLOG(1) << "Q: \n" << qp_params.Q;
  VLOG(1) << "d: \n" << qp_params.d;

  Eigen::Matrix<double, 2, 1> x_result;
  x_result(0) = tao / 2;
  x_result(1) = 1 - x_result(0);
  double min_value;
  bool qp_flag = qp_solver(qp_params, &x_result, &min_value);
  LOG(INFO) << "estimated inlier_ratio=" << x_result(0)
            << " termination_type: " << qp_flag;
  params->inlier_ratio = x_result(0);

  // Calculate posterior and final weights.
  prob->resize(nqry_kpts);
  for (int i = 0; i < nqry_kpts; i++) {
    // Calculate posterior.
    const double gam_val =
        x_result(0)*GammaPdf(first_distances[i], params->k, params->theta);
    const double gev_val =
        (1.0 - x_result(0))*GEVPdf(first_distances[i], params->mu,
                                   params->sigma, params->xi);
    const double posterior = gam_val / (gam_val + gev_val);
    // Removing those matches that are likely to be incorrect.
    (*prob)[i] = prediction_results[i] ? static_cast<float>(posterior) : 0.0f;
  }
}
