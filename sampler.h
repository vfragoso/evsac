// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef SAMPLER_H_
#define SAMPLER_H_

#include <random>
#include <unordered_set>
#include <vector>

// Random sampler for ransac scheme.
class RandomSampler {
 public:
  // Constructs an instance of RandomSampler.
  // Parameters:
  //    confidences  A probability for every correspondence indicating how
  //                 likely it is of being a good correspondence (inlier).
  explicit RandomSampler(const std::vector<float>& confidences) :
      die_(confidences.begin(), confidences.end()) {
    std::random_device rd;
    rng_.seed(rd());
  }

  virtual ~RandomSampler() {}

  // Sample operation. It selects a bunch of indices following the confidences
  // passed when constructing an instance of RandomSampler.
  // Parameters:
  //   sdata  The sampled indices of the data.
  virtual void Sample(std::vector<int>* sdata) {
    // Assuming sdata is not null (to avoid checking it every time we sample)
    // and that it has the size of the minimal size for the model to compute.
    std::unordered_set<int> idxs;
    uint i = 0;
    while (idxs.size() != sdata->size()) {
      int idx = die_(rng_);  // Sample
      auto it = idxs.find(idx);
      if (it != idxs.end()) continue;  // Already in idxs
      idxs.insert(idx);
      (*sdata)[i++] = idx;
    }
  }

 private:
  // The discrete distribution.
  std::discrete_distribution<> die_;
  // The random number generator.
  std::mt19937 rng_;
};
#endif  // SAMPLER_H_
