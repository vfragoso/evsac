// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef HOMO_H_
#define HOMO_H_

#include <vector>
#include "Eigen/Core"
#include "opencv2/core/core.hpp"
#include "model.h"

// Class encapsulating homography for OpenCV's case. Implements the Model class
// for estimating homography.
// first: ref kpt.
// second: qry kpt.
class Homography : public Model<cv::Point2f> {
 public:
  // Constructor
  Homography(void) : Model<cv::Point2f>(8, 4) {}

  // Destructor
  virtual ~Homography() {}

  // Computes a homography.
  bool Compute(const std::vector<cv::Point2f>& ref_samples,
               const std::vector<cv::Point2f>& qry_samples,
               const std::vector<int>& sdata,
               Eigen::MatrixXd* homography) const override;

  // Calculates residuals for homography.
  bool CalculateResiduals(const std::vector<cv::Point2f>& ref_samples,
                          const std::vector<cv::Point2f>& qry_samples,
                          const Eigen::MatrixXd& homography,
                          std::vector<double>* residuals) const override {
    if (ref_samples.size() != qry_samples.size() || !residuals) return false;
    residuals->resize(ref_samples.size());
    for (int i = 0; i < ref_samples.size(); i++) {
      Eigen::Vector3d ref_vec;
      ref_vec(0) = static_cast<double>(ref_samples[i].x);
      ref_vec(1) = static_cast<double>(ref_samples[i].y);
      ref_vec(2) = 1.0;
      Eigen::Vector3d mapped_ref_kpt = homography*ref_vec;
      const double& x = mapped_ref_kpt(0);
      const double& y = mapped_ref_kpt(1);
      const double& z = mapped_ref_kpt(2);
      const double x_error = qry_samples[i].x - x / z;
      const double y_error = qry_samples[i].y - y / z;
      (*residuals)[i] = sqrt(x_error*x_error + y_error*y_error);
    }
    return true;
  }

  // Implementation of LM refinement.
  bool Refine(const std::vector<cv::Point2f>& ref_samples,
              const std::vector<cv::Point2f>& qry_samples,
              const std::vector<int>& sdata,
              Eigen::MatrixXd* homography) const override {
    // Refining using OpenCV.
    const int n = sdata.size();
    std::vector<cv::Point2f> ref_sample(n);
    std::vector<cv::Point2f> qry_sample(n);
    for (int k = 0; k < n; k++) {
      ref_sample[k] = ref_samples[sdata[k]];
      qry_sample[k] = qry_samples[sdata[k]];
    }
    cv::cv2eigen(cv::findHomography(ref_sample, qry_sample),
                 *CHECK_NOTNULL(homography));
    return true;
  }
};

// Implementation of homography computation.
bool Homography::Compute(const std::vector<cv::Point2f>& ref_samples,
                         const std::vector<cv::Point2f>& qry_samples,
                         const std::vector<int>& sdata,
                         Eigen::MatrixXd* homography) const {
  if (ref_samples.size() != qry_samples.size() ||
      sdata.size() < min_samples() || !homography) return false;
  // A => Matrix holding the correspondences.
  // a_y = [0 0 0 -x -y -1  y'*x  y'*y  y']
  // a_x = [x y 1 0 0 0  -x'*x  -x'*y  -x']
  // A = [a_x1; a_y1]
  // Compute A^T*A and compute its singular values. The eigenvector
  // associated to the smallest eigenvalue is the solution.
  // A^T*A is always positive semidefinite, thus do an LDL factorization.
  // x' = H*x, x = [x y 1]
  // Ah = 0
  const int num_samples = sdata.size();
  Eigen::MatrixXd A(2*num_samples, 9);
  A.setZero();

  // Fill the matrix A with correspondences.
  for (int i = 0; i < num_samples; ++i) {
    const int a_row = 2*i;
    const cv::Point2f& ref_pt = ref_samples[sdata[i]];
    const cv::Point2f& qry_pt = qry_samples[sdata[i]];

    // Computing row a_y.
    A(a_row, 3) = -ref_pt.x;
    A(a_row, 4) = -ref_pt.y;
    A(a_row, 5) = -1.0;
    A(a_row, 6) = qry_pt.y * ref_pt.x;
    A(a_row, 7) = qry_pt.y * ref_pt.y;
    A(a_row, 8) = qry_pt.y;

    // Computing row a_x.
    A(a_row + 1, 0) = ref_pt.x;
    A(a_row + 1, 1) = ref_pt.y;
    A(a_row + 1, 2) = 1.0;
    A(a_row + 1, 6) = -qry_pt.x * ref_pt.x;
    A(a_row + 1, 7) = -qry_pt.x * ref_pt.y;
    A(a_row + 1, 8) = -qry_pt.x;
  }

  Eigen::MatrixXd AA = A.transpose()*A;
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(AA);
  const Eigen::VectorXd& eig_val = eig.eigenvalues();
  const Eigen::MatrixXd& eig_vec = eig.eigenvectors();

  // Select the smallest eigenvalue and the result is the eigenvector w/ the
  // smallest eigenvalue.
  int idx = 0;
  double min_eigval = eig_val(0);
  for (int i = 1; i < 9; ++i) {
    if (min_eigval > eig_val(i)) {
      min_eigval = eig_val(i);
      idx = i;
    }
  }

  // This copies the calculated model into h (assuming h is ColMajor).
  auto& result = eig_vec.col(idx);
  double* homography_data = homography->data();
  for (int i = 0; i < 9; ++i) homography_data[i] = result(i) / result(8);
  // If RowMajor this is not necessary.
  homography->transposeInPlace();
  return true;
}
#endif  // HOMO_H_
