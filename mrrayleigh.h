// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef MRRAYLEIGH_H_
#define MRRAYLEIGH_H_

#include <cmath>
#include <vector>
#include "statx/distributions/rayleigh.h"

// Meta-Recognition Rayleigh for distances. Predicts if a correspondences is
// correct or incorrect given the distances used when matching features.
// Parameters:
//   distances  A container holding a sorted vector of the distances between a
//              query and every reference descriptor.
//   p  Percentile for defining tail (usually < 5% of the data is fine). This
//      will be used to grab only the 0.5% of the distance vector for
//      prediction.
//   eps  Confidence threshold to declare a correspondence as correct one.
//        This confidence is within the interval of 0 and 1.
inline bool mrrayleigh(const std::vector<float>& distances,
                       const float p = 0.005f,
                       const float eps = 0.60) {
  // Calculate tail size according to p.
  const int k = std::round(p*distances.size());
  std::vector<double> tail(distances.begin() + 1, distances.begin() + k + 1);
  // Fit tail!
  const double sigma = statx::FitRayleigh(tail);
  // Calculate belief of correctness.
  double conf = 1.0 - statx::RayleighCdf(distances[0], sigma);
  // Predict!
  return conf >= eps;
}
#endif  // MRRAYLEIGH_H_
