// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef FUNDMAT_H_
#define FUNDMAT_H_

#include <vector>
#include "Eigen/Core"
#include "Eigen/SVD"
#include "opencv2/core/core.hpp"
#include "model.h"

// Class to estimate the fundamental matrix using 8pt algorithm. Implements the
// Model class for estimating fundamental matrix.
class FundamentalMatrix : public Model<cv::Point2f> {
 public:
  // Constructs a fundamental matrix estimator.
  FundamentalMatrix(void) : Model<cv::Point2f>(9, 8) {}

  virtual ~FundamentalMatrix() {}

  // Computes the fundamental matrix.
  bool Compute(const std::vector<cv::Point2f>& ref_samples,
               const std::vector<cv::Point2f>& qry_samples,
               const std::vector<int>& sdata,
               Eigen::MatrixXd* fundamental_mat) const override;

  // Calculates the residuals for a given model.
  bool CalculateResiduals(const std::vector<cv::Point2f>& ref_samples,
                          const std::vector<cv::Point2f>& qry_samples,
                          const Eigen::MatrixXd& fundamental_mat,
                          std::vector<double>* residuals) const override;

  // Refines a given model.
  bool Refine(const std::vector<cv::Point2f>& ref_samples,
              const std::vector<cv::Point2f>& qry_samples,
              const std::vector<int>& sdata,
              Eigen::MatrixXd* fundamental_mat) const override;

 protected:
  // Normalizes points.
  void NormalizePoints(const std::vector<cv::Point2f>& pts,
                       std::vector<Eigen::Vector2d>* norm_pts,
                       Eigen::Matrix3d* t) const;
};

//------------------ Implementation --------------------

// 8pt algorithm
// x'^T F x = 0  Epipolar constraint
// x' = [x' y' 1]^T => Query point
// x = [x y 1]^T => Reference
// Af = 0, where A is built from the correspondences
// Ai = [x'x  x'y  x'  y'x  y'y  y'  x  y  1]
//
// Thus, build matrix A with at least 8 correspondences
// Rankf of A should be 8 in the noise-free case
// We have to enforce Rank 2 of the Fundamental Matrix
// The asiest solution is to eliminate the smallest eigenvalue and reconstruct
// from the two remaining stronger eigenvalues and eigenvectors.
// F' = U diag(sigma1, sigma2, 0) V^T
// where U and V are the orthogonal basis for the left and right spaces
// and sigma_i are the singular values.
// For this we need to normalize the points as described in Multiple View
// Geometry from Hartley and Zisserman (page 282).
// Note on Theia: Chris is not taking RMS, and possible not calculating the
// null-space correctly.
bool FundamentalMatrix::Compute(const std::vector<cv::Point2f>& ref_samples,
                                const std::vector<cv::Point2f>& qry_samples,
                                const std::vector<int>& sdata,
                                Eigen::MatrixXd* f) const {
  if (ref_samples.size() != qry_samples.size() ||
      sdata.size() < min_samples() || !f) return false;
  const int n = sdata.size();
  Eigen::MatrixXd A(n, 9);

  // Normalize points.
  std::vector<Eigen::Vector2d> ref_pts;
  std::vector<Eigen::Vector2d> qry_pts;
  Eigen::Matrix3d t1;
  Eigen::Matrix3d t2;
  NormalizePoints(ref_samples, &ref_pts, &t1);
  NormalizePoints(qry_samples, &qry_pts, &t2);

  for (int i = 0; i < n; ++i) {
    const Eigen::Vector2d& ref_pt = ref_pts[sdata[i]];
    const Eigen::Vector2d& qry_pt = qry_pts[sdata[i]];

    A(i, 0) = qry_pt.x() * ref_pt.x();
    A(i, 1) = qry_pt.x() * ref_pt.y();
    A(i, 2) = qry_pt.x();
    A(i, 3) = qry_pt.y() * ref_pt.x();
    A(i, 4) = qry_pt.y() * ref_pt.y();
    A(i, 5) = qry_pt.y();
    A(i, 6) = ref_pt.x();
    A(i, 7) = ref_pt.y();
    A(i, 8) = 1.0;
  }

  // Calculate F from the null-space.
  Eigen::MatrixXd AA = A.transpose()*A;
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(AA);
  const Eigen::VectorXd& eig_val = eig.eigenvalues();
  const Eigen::MatrixXd& eig_vec = eig.eigenvectors();

  // Select the smallest eigenvalue and the result is the eigenvector w/ the
  // smallest eigenvalue.
  int idx = 0;
  double min_eigval = eig_val(0);
  for (int i = 1; i < 9; ++i) {
    if (min_eigval > eig_val(i)) {
      min_eigval = eig_val(i);
      idx = i;
    }
  }

  // This copies the calculated model into f (assuming f is ColMajor).
  auto& result = eig_vec.col(idx);
  Eigen::Matrix3d f_mat;
  double* f_data = f_mat.data();
  for (int i = 0; i < 9; ++i) f_data[i] = result(i);
  // If RowMajor this is not necessary.
  f_mat.transposeInPlace();

  // Calculate F' = U diag(sigma1, sigma2, 0) V^T.
  auto flag =  Eigen::ComputeFullU | Eigen::ComputeFullV;
  Eigen::JacobiSVD<Eigen::Matrix3d> svd(f_mat, flag);
  Eigen::Vector3d singular_values = svd.singularValues();
  singular_values[2] = 0.0;
  Eigen::Matrix3d f_prime =
      svd.matrixU() * singular_values.asDiagonal() * svd.matrixV().transpose();
  // Calculate final fundamental matrix.
  *f = t2.transpose() * f_prime * t1;
  return true;
}

// Calculates residuals.Since the 8pt algorithm solves for f in a LS sense, we
// use the same LS residual for every correspondence, i.e., x'^T F x = r.
bool FundamentalMatrix::CalculateResiduals(
    const std::vector<cv::Point2f>& ref_samples,
    const std::vector<cv::Point2f>& qry_samples,
    const Eigen::MatrixXd& fundamental_mat,
    std::vector<double>* residuals) const {
  if (ref_samples.size() != qry_samples.size()) return false;

  const int n = ref_samples.size();
  residuals->resize(n);

  for (int i = 0; i < n; ++i) {
    const cv::Point2f& ref_pt = ref_samples[i];
    const cv::Point2f& qry_pt = qry_samples[i];
    const Eigen::Vector3d ref(ref_pt.x, ref_pt.y, 1.0);
    const Eigen::Vector3d qry(ref_pt.x, ref_pt.y, 1.0);
    (*residuals)[i] = qry.dot(fundamental_mat * ref);
  }
  return true;
}

// Refinement of a fundamental matrix.
bool FundamentalMatrix::Refine(const std::vector<cv::Point2f>& ref_samples,
                               const std::vector<cv::Point2f>& qry_samples,
                               const std::vector<int>& sdata,
                               Eigen::MatrixXd* fundamental_mat) const {
  // TODO(vfragoso): Use opencv's function for this.
  const int n = sdata.size();
  std::vector<cv::Point2f> ref_sample(n);
  std::vector<cv::Point2f> qry_sample(n);
  for (int k = 0; k < n; k++) {
    ref_sample[k] = ref_samples[sdata[k]];
    qry_sample[k] = qry_samples[sdata[k]];
  }
  cv::Mat f = cv::findFundamentalMat(ref_sample, qry_sample, CV_FM_8POINT);
  cv::cv2eigen(f, *fundamental_mat);
  return true;
}

void FundamentalMatrix::NormalizePoints(const std::vector<cv::Point2f>& pts,
                                        std::vector<Eigen::Vector2d>* norm_pts,
                                        Eigen::Matrix3d* t) const {
  const int n = pts.size();
  norm_pts->resize(n);
  Eigen::Vector2d centroid;
  centroid.setZero();

  // Calculate centroid.
  for (const cv::Point2f& pt : pts) {
    centroid += Eigen::Vector2d(pt.x, pt.y);
  }
  centroid /= n;

  // Calculate distance to centroid.
  double distance = 0.0;
  for (const cv::Point2f& pt : pts) {
    distance += (Eigen::Vector2d(pt.x, pt.y) - centroid).squaredNorm();
  }
  distance /= n;
  // RMS distance!
  distance = sqrt(distance);

  // Calculate transformation matrix t.
  const double norm_factor = sqrt(2) / distance;
  *t << norm_factor, 0.0,  -1.0 * norm_factor * centroid.x(),
      0.0, norm_factor, -1.0 * norm_factor * centroid.y(),
      0.0, 0.0, 1.0;

  // Transform points
  for (int i = 0; i < n; ++i) {
    const cv::Point2f& pt = pts[i];
    (*norm_pts)[i] = t->block<2, 3>(0, 0) * Eigen::Vector3d(pt.x, pt.y, 1.0);
  }
}

#endif  // FUNDMAT_H_
