// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

// This binary uses EVSAC to calculate homography or fundamental matrix by using
// a simple RANSAC implementation. It uses OpenCV to read images and compute the
// features and descriptors for matching.
//
// ./evsac_example
// -ref_img=/path/to/ref_img.png
// -qry_img=/path/to/qry_img.png
// -descriptor=SIFT
// -matcher=BruteForce
// -nkpts=1500
// -fit_method=0
// -homo=true

#include <vector>
#include "glog/logging.h"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "evsac.h"

DEFINE_string(ref_img, "./ref_img.png", "Reference image");
DEFINE_string(qry_img, "./qry_img.png", "Query image");
DEFINE_string(descriptor, "SIFT", "Descriptor: SIFT or SURF");
DEFINE_string(matcher, "BruteForce", "OpenCV Descriptor Matcher");
DEFINE_int64(nkpts, 1500, "Number of keypoints");
DEFINE_int64(fit_method, 0, "Fit Method: 0-MLE, 1-QuantileLeastSquares");
DEFINE_bool(homo, true,
            "Calculate homography (true) / fundamental mat (false)");

int main(int argc, char** argv) {
  using cv::KeyPoint;
  using cv::Mat;
  using std::vector;

  google::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);

  // Logging input parameters.
  LOG(INFO) << "ref_img: " << FLAGS_ref_img;
  LOG(INFO) << "qry_img: " << FLAGS_qry_img;
  LOG(INFO) << "nkpts: " << FLAGS_nkpts;
  LOG(INFO) << "matcher: " << FLAGS_matcher;
  LOG(INFO) << "descriptor: " << FLAGS_descriptor;

  // Loading images.
  Mat ref = cv::imread(FLAGS_ref_img, CV_LOAD_IMAGE_GRAYSCALE);
  Mat qry = cv::imread(FLAGS_qry_img, CV_LOAD_IMAGE_GRAYSCALE);

  // Check that we read the images.
  if (ref.empty() || qry.empty()) {
    LOG(INFO) << "Cannot load image(s)= ref:" << FLAGS_ref_img
              << " qry: " << FLAGS_qry_img;
    return -1;
  }

  // Create the feature detector.
  cv::Ptr<cv::FeatureDetector> detector_ptr =
      cv::FeatureDetector::create(FLAGS_descriptor);

  if (!detector_ptr) {
    LOG(INFO) << "Unable to create keypoint detector";
    return -1;
  }

  // Create a GridAdaptedFeatureDetector to limit the number of detected kpts.
  cv::GridAdaptedFeatureDetector detector(detector_ptr, FLAGS_nkpts);

  // Detect features on both images.
  vector<KeyPoint> ref_kpts, qry_kpts;
  detector.detect(ref, ref_kpts);
  detector.detect(qry, qry_kpts);

  LOG(INFO) << "Ref Kpts: " << ref_kpts.size();
  LOG(INFO) << "Qry Kpts: " << qry_kpts.size();

  // Create the descriptor extractor.
  cv::Ptr<cv::DescriptorExtractor> extractor =
      cv::DescriptorExtractor::create(FLAGS_descriptor);

  if (!extractor) {
    LOG(INFO) << "Unable to create descriptor extractor";
    return -1;
  }

  // Compute descriptors.
  Mat ref_desc, qry_desc;
  extractor->compute(ref, ref_kpts, ref_desc);
  extractor->compute(qry, qry_kpts, qry_desc);

  LOG(INFO) << "Reference descriptor matrix: " << ref_desc.size();
  LOG(INFO) << "Query descriptor matrix: " << qry_desc.size();

  // Match the features (ref <- qry).
  cv::Ptr<cv::DescriptorMatcher> matcher =
      cv::DescriptorMatcher::create(FLAGS_matcher);

  if (!matcher) {
    LOG(INFO) << "Unable to create descriptor matcher";
    return -1;
  }

  LOG(INFO) << "Matching...";
  vector<vector<cv::DMatch> > match_results;
  matcher->knnMatch(qry_desc, ref_desc, match_results, ref_kpts.size());

  // Getting the Points from KeyPoint structures.
  vector<cv::Point2f> ref_pts, qry_pts;
  for (const KeyPoint& kpt : ref_kpts) {
    ref_pts.emplace_back(kpt.pt.x, kpt.pt.y);
  }

  for (const KeyPoint& kpt : qry_kpts) {
    qry_pts.emplace_back(kpt.pt.x, kpt.pt.y);
  }

  // Pass the matching results to EVSAC.
  LOG(INFO) << "EVSAC ...";
  const EVSAC::FitMethod fit_method =
      (FLAGS_fit_method == 0) ? EVSAC::MLE : EVSAC::QUANTILE_LS;
  EVSAC evsac(fit_method);
  EVSAC::MixtureModelParams mix_params;

  // Compute homography.
  if (FLAGS_homo) {
    Eigen::MatrixXd homography(3, 3);
    evsac.ComputeHomography(match_results, ref_pts, qry_pts,
                            &mix_params, &homography);
    LOG(INFO) << "Homography: \n" << homography;
    return 0;
  }

  // Compute fundamental matrix.
  Eigen::MatrixXd fmat(3, 3);
  evsac.ComputeFundamentalMatrix(match_results, ref_pts, qry_pts,
                                 &mix_params, &fmat);
  LOG(INFO) << "Fundamental Matrix: \n" << fmat;
  return 0;
}
