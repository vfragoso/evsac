function testEVSAC()
% testEVSAC: An example invoking the EVSAC function
%

% 1. Read Matching Data
msfstr = './test/trees_mscores6_SIFT.txt';
mgtfstr = './test/trees_matchinfo6_SIFT.txt';

% mscores: Matching scores matrix
% pgt: vector indicating which row is a correct correspondence
[mscores, pgt] = getData(msfstr, mgtfstr);

% 2. Call EVSAC
[w, pos, inlr_ratio, gamp, gevp] = evsac(mscores);

% 3. Truth Inlier Ratio
inlier_ratio = numel(find(pgt==1))/numel(pgt);

fprintf('Estimated Inlier Ratio: %1.3d\n', inlr_ratio);
fprintf('True Inlier Ratio: %1.3d\n', inlier_ratio);

% Checking visually the mixture model
[F, X] = ecdf(mscores(:, 1));
Fgam = gamcdf(X, gamp(1), gamp(2));
% Fgev = gevcdf(X, gevp(1), gevp(2), gevp(3)); % For maxima (e.g.,
% similarity scores)
Fgev = 1 - gevcdf(-X, gevp(1), gevp(2), gevp(3)); % For minima (e.g., distances)
Fhat = inlr_ratio*Fgam + (1 - inlr_ratio)*Fgev;

% Visualizing the empirical and estimated CDF Models
close all;
figure; hold on;
plot(X, F, 'LineWidth', 1.5);
plot(X, Fhat, 'Color', 'r', 'LineWidth', 1.5);
xlabel('Distance (1 NN)');
ylabel('CDF');
legend('Empirical', 'Estimated', 'Location', 'Best');
hold off;

% Weights w or pos can be used to sample correspondences.
% In the paper, we report the results obtained by using the weights w.
figure;
subplot(3, 1, 1), bar(pgt);
subplot(3, 1, 2), bar(w);
subplot(3, 1, 3), bar(pos);

hdist_w = norm(sqrt(pgt) - sqrt(w))/sqrt(2)
hdist_pos = norm(sqrt(pgt) - sqrt(pos))/sqrt(2)
end

% Reads Matching data 
function [m, pgt] = getData(msfstr, mgtfstr)
m = csvread(msfstr);
minfo = csvread(mgtfstr);
pgt = minfo(:, 4);
end