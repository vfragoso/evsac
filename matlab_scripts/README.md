Copyright (C) 2014 Victor Fragoso <vfragoso@cs.ucsb.edu>
--------------------------------------------------------
EVSAC
--------------------------------------------------------

1.0 Introduction

EVSAC is another *sampling* method for estimations from feature correspondences 
using a hypothesis-test-loop.

2.0 MATLAB Scripts

The directory `matlab_scripts' contains three MATLAB scripts.

    1. evsac.m: Script implementing the computation of the sampling weights 
       *ONLY*. These weights can be used in a RANSAC implementation for 
       producing hypotheses.

    2. MRRayleigh.m: Implements a Meta-Recognition based correctness predictor
       of feature correspondences.

    3. testEVSAC.m: Test case of EVSAC w/o doing any fitting. The purpose is
       to show how to invoke and use *evsac.m*. 

Also, a directory `matlab_scripts/test' contains matching information between
two images from the Trees-Oxford dataset obtained using SIFT from OpenCV.

3.0 Citation

If this work is used in any project, please cite our paper:

@InProceedings{Fragoso_2013_ICCV,
author = {Victor Fragoso and Pradeep Sen and Sergio Rodriguez and Matthew Turk},
title = {{EVSAC}: {A}ccelerating {H}ypotheses {G}eneration by {M}odeling {M}atching {S}cores with {E}xtreme {V}alue {T}heory},
booktitle = {Proc. of IEEE International Conference on Computer Vision (ICCV)},
month = {December},
year = {2013}
}

4.0 Notes: A C++11 version of EVSAC is on its way.