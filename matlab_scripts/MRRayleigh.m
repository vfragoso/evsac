function [p, raylc] = MRRayleigh(mscores, k, thld)
% MRRayleigh: A Meta-Recognition predictor for correctness of feature 
% matches. 
% 
% [p, raylc] = MRRayleigh(mscores, k, thld)
% Input Arguments.
%   mscores: NxM Matrix holding the euclidean distances of every query 
%   descriptor and all the M reference descriptors. The i-th row
%   contains the distances of the i-th query descriptor and every reference
%   descriptor in ascending order.
%   
%   k: The number of elements used for computing the prediction. As a rule
%   of thumb, k = round(M*0.005), where M is the number of reference
%   descriptors and M >= 1000.
%
%   thld: Threshold used to predict correctness. The threshold is in the
%   range of [0 - 1]. Good results were obtained with thld=0.6.
%
% Output.
%   p: It is the prediction vector. The i-th entry is the prediction for 
%   the i-th query descriptor, i.e., for the i-th row of the matrix mscores.
%   A 1 is set when a match/correspondence is predicted as correct, and 0
%   otherwise.
%
%   raylc: It is the confidence vector. The i-th entry of this vector holds
%   a confidence value for the i-th row of the matrix mscores. The
%   confidence is in the range of [0 - 1].
%
% This script implements the predictor proposed in:
% V. Fragoso and M. Turk. SWIGS: A Swift Guided Sampling Method. CVPR 2008.

n = size(mscores, 1);  % Number of correspondences
p = zeros(n, 1);  % Binary predictions
raylc = zeros(n, 1);  % Confidence vector

% Predict for every correspondence
for i=1:n
    raylparams = raylfit(mscores(i,2:k+1));
    conf = 1 - raylcdf(mscores(i, 1), raylparams);
    raylc(i) = conf;
    if conf >= thld
        p(i) = 1;
    else
        p(i) = 0;
    end
end
end