function [w, pos, inlr_ratio, gamp, gevp] = evsac(mscores)
% evsac: Generating a sampling strategy for robust model fitting.
%
% [w, pos, inlr_ratio, gamp, gevp] = evsac(mscores)
% Input Arguments.
%   mscores: NxM Matrix holding the euclidean distances of every query 
%   descriptor and all the M reference descriptors. The i-th row
%   contains the distances of the i-th query descriptor and every reference
%   descriptor in ascending order.
%
% Output.
%   w: The weights used for generating hypotheses in a
%   hypothesis-test-loop. These weights discard those matches that were
%   predicted as incorrect matches.
% 
%   pos: Weights computed using the posterior for correct matches given the
%   mixture model. These weights can also be used for generating
%   hypotheses, however, these weights consider all the correspondences,
%   which can cause a slower convergence.
%
%   inlr_ratio: The inlier ratio estimate given the computed mixture model.
%
%   gamp: Parameters of the Gamma distribution
%
%   gevp: Parameters of the GEV distribution

n = size(mscores, 1);
m = size(mscores, 2);

% 1. Predict
k = round(m*0.005);
thld = 0.6;
[p, ~] = MRRayleigh(mscores, k, thld);

% 2. Find model parameters
vidx = find(p==1);  % Which rows were predicted as correct?
mv = mscores(vidx, 1);  % 1NN mscore
mnv = mscores(:, 2);  % 2NN mscore

% Fit Gamma to the predicted correct matching scores
gamp = gamfit(mv);

% Fit GEV to the 2NN matching scores (as discussed in the paper)
%gevp = gevfit(mnv);  % For maxima (e.g., similarity scores)
gevp = gevfit(-mnv);  % For minima (e.g., distances)

% Estimate inlier ratio
[F, X] = ecdf(mscores(:,1));

Fin = gamcdf(X, gamp(1), gamp(2));
%Fou = gevcdf(X, gevp(1), gevp(2), gevp(3)); % For maxima (e.g., similarity
%scores)
Fou = 1 - gevcdf(-X, gevp(1), gevp(2), gevp(3)); % For minima (e.g, distances)

% Solve the Constrained least squares
C = [Fin Fou];
epsilon_hat = lsqlin(C, F, eye(2), [numel(vidx)/n; 1], ...
    [1 1], [1], [0 0], [1 1]);
inlr_ratio = epsilon_hat(1);

% Calculate Weights for sampling
cin = inlr_ratio*gampdf(mscores(:, 1), gamp(1), gamp(2));
%cou = (1 - inlr_ratio)*gevpdf(mscores(:, 1), gevp(1), gevp(2), gevp(3)); % For maxima (e.g., similarity
cou = (1 - inlr_ratio)*gevpdf(-mscores(:, 1), gevp(1), gevp(2), gevp(3)); % For minima (e.g, distances)

% 3. Produce the weights
% Posterior weights
pos = cin ./ (cin + cou);

% Weights for sampling ignoring those labeld as incorrect correspondences
w = p .* pos;
end