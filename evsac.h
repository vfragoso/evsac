// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef EVSAC_H_
#define EVSAC_H_

#include <vector>

#include "Eigen/Core"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"

#include "sampler.h"

// Implementation for OpenCV.
class EVSAC {
 public:
  // Defines the two methods for computing the parameters of the involved
  // distributions, i.e., Gamma and GEV.
  // MLE: Maximum likelihood estimation.
  // QUANTILE_LS: Quantile Least-Squares problem.
  enum FitMethod {MLE = 0, QUANTILE_LS = 1};

  // Parameters for the mixture model. The mixture model considers a Gamma and
  // a GEV distribution.
  struct MixtureModelParams {
    // Gamma parameters.
    // shape.
    double k;
    // scale.
    double theta;
    // GEV parameters.
    // tail parameter (shape).
    double xi;
    // scale parameter.
    double sigma;
    // location.
    double mu;
    // Estimated inlier ratio.
    double inlier_ratio;
  };

  // Constructs an instance of EVSAC.
  // Parameters:
  //   fit_method  The method used for estimating the distribution parameters.
  //   p  A threshold used to calculate the number of samples that define the
  //      tail of the process generating distances for incorrect
  //      correspondences.
  //   eps  The confidence threshold for the predictor.
  EVSAC(FitMethod fit_method = MLE,
        float p = 0.005f,
        float eps = 0.60) : fit_method_(fit_method), p_(p), eps_(eps) {}

  virtual ~EVSAC(void) {}

  // Computes a homography from putative correspondences.
  // Parameters:
  //   match_results  The matching results from OpenCV.
  //   ref_kpts  The reference keypoints.
  //   qry_kpts  The query keypoints.
  //   mixture_model_params  The estimated mixture model parameters.
  //   homography  The estimated homography.
  void ComputeHomography(
      const std::vector<std::vector<cv::DMatch> >& match_results,
      const std::vector<cv::Point2f>& ref_kpts,
      const std::vector<cv::Point2f>& qry_kpts,
      EVSAC::MixtureModelParams* mixture_model_params,
      Eigen::MatrixXd* homography);

  // Computes a fundamental matrix from putative correspondences.
  // Parameters:
  //   match_results  The matching results from OpenCV.
  //   ref_kpts  The reference keypoints.
  //   qry_kpts  The query keypoints.
  //   mixture_model_params  The estimated mixture model parameters.
  //   fundamental_matrix  The estimated fundamental_matrix.
  void ComputeFundamentalMatrix(
      const std::vector<std::vector<cv::DMatch> >& match_results,
      const std::vector<cv::Point2f>& ref_kpts,
      const std::vector<cv::Point2f>& qry_kpts,
      EVSAC::MixtureModelParams* mixture_model_params,
      Eigen::MatrixXd* fundamental_matrix);

 protected:
  // Calculates mixture model given the matching distances.
  void CalculateMixtureModel(
      const std::vector<std::vector<cv::DMatch> >& match_results,
      EVSAC::MixtureModelParams* params,
      std::vector<float>* prob);

 private:
  // The fiting method.
  const FitMethod fit_method_;
  // Predictor params.
  // Percentile for computing tail.
  const double p_;
  // Threshold for prediction (correct/incorrect correspondence).
  const double eps_;
};

#endif  // EVSAC_H_
