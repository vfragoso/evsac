// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef MODEL_H_
#define MODEL_H_

#include <vector>

#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "opencv2/core/core.hpp"
#include "opencv2/core/eigen.hpp"
#include "opencv2/calib3d/calib3d.hpp"

// Class encapsulating model estimators (e.g., homography).
template <typename Data> class Model {
 public:
  // Constructs an instance of Model.
  // Parameters:
  //   n_params  Number of parameters to estimate.
  //   min_samples  Number of minimal samples for computing an estimate.
  Model(const uint n_params, const uint min_samples) :
      n_params_(n_params), min_samples_(min_samples) {}

  virtual ~Model() {}

  // Computes the model given the data.
  // Parameters:
  //   ref_samples  The reference samples.
  //   qry_samples  The query samples.
  //   sampled_data_idx  Indices of the data to use for the estimation.
  //   model  The estimated model.
  virtual bool
  Compute(const std::vector<Data>& ref_samples,
          const std::vector<Data>& qry_samples,
          const std::vector<int>& sampled_data_idx,
          Eigen::MatrixXd* model) const = 0;

  // Calculates the residuals of the model given some correspondences.
  // Parameters:
  //   ref_samples  The reference samples.
  //   qry_samples  The query samples.
  //   model  The estimated model.
  //   residuals  The computed residuals.
  virtual bool
  CalculateResiduals(const std::vector<Data>& ref_samples,
                     const std::vector<Data>& qry_samples,
                     const Eigen::MatrixXd& model,
                     std::vector<double>* residuals) const = 0;

  // Refines the estimated model.
  // Parameters:
  //   ref_samples  The reference samples.
  //   qry_samples  The query samples.
  //   sampled_data_idx  Indices of the data to use for the estimation.
  //   model  The estimated model.
  virtual bool
  Refine(const std::vector<Data>& ref_samples,
         const std::vector<Data>& qry_samples,
         const std::vector<int>& sampled_data_idx,
         Eigen::MatrixXd* model) const = 0;

  // Getters.
  inline int n_params() const {
    return n_params_;
  }

  inline int min_samples() const {
    return min_samples_;
  }

 private:
  // Number of parameters to estimate.
  const uint n_params_;
  // Number of the minimum samples for computing a model estimate.
  const uint min_samples_;
};
#endif  // MODEL_H_
