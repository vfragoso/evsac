// Copyright (C) 2014  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef RANSAC_H_
#define RANSAC_H_

#include <vector>
#include "Eigen/Core"
#include "model.h"
#include "sampler.h"

// A minimalistic RANSAC class.
class Ransac {
 public:
  // Constructs an instance of the Ransac method.
  // Parameters:
  //   p  The probability of success.
  //   eps  The threshold used for deciding if a sample is inlier.
  //   alpha  Desired or satisfactory inlier ratio.
  //   adaptive  Calculate the number of iterations adaptively.
  //   max_iters  The number of maximum iterations.
  Ransac(const double p = 0.95,
         const double eps = 3.0,
         const double alpha = 0.5,
         const bool adaptive = true,
         const int max_iters = 1000) :
      p_(p), eps_(eps), alpha_(alpha),
      adaptive_(adaptive), max_iters_(max_iters) {}

  // Destructor
  virtual ~Ransac() {}

  // Estimate Model
  template <typename Data>
  bool Estimate(const Model<Data>& model_estimator,
                RandomSampler& sampler,
                const std::vector<Data>& ref_samples,
                const std::vector<Data>& qry_samples,
                Eigen::MatrixXd* model,
                std::vector<bool>* inliers);

 private:
  // Probability of success.
  const double p_;
  // Threshold for deciding inliers/outliers.
  const double eps_;
  // Desired/Satisfactory inlier ratio.
  const double alpha_;
  // Adaptive computation of iterations.
  const bool adaptive_;
  // Maximum number of iterations.
  const int max_iters_;
};

//------------------ Implementation -----------------------
template <typename Data>
bool Ransac::Estimate(const Model<Data>& model_estimator,
                      RandomSampler& sampler,
                      const std::vector<Data>& ref_samples,
                      const std::vector<Data>& qry_samples,
                      Eigen::MatrixXd* model,
                      std::vector<bool>* inliers) {
  if (ref_samples.empty() ||
      ref_samples.size() != qry_samples.size() ||
      !model || !inliers) {
    LOG(INFO) << "Invalid arguments";
    return false;
  }

  // Initialize.
  uint iter = 0;
  uint max_iterations = max_iters_;
  const int nsamples = ref_samples.size();
  inliers->resize(nsamples);
  const int min_samples = model_estimator.min_samples();
  std::vector<int> sdata(min_samples);
  std::vector<double> residuals(nsamples);
  Eigen::MatrixXd bestModel;
  int support = 0;
  int best_support = 0;
  const double log_eta = log(1.0 - p_);
  double inlier_ratio = 0.0;
  LOG(INFO) << "ncorrespondences: " << nsamples;

  // Hypothesis-and-test loop.
  while (iter++ < max_iterations) {
    // 1. Sample data for model generation.
    sampler.Sample(&sdata);

    // 2. Generate model (hypothesis).
    if (!model_estimator.Compute(ref_samples, qry_samples, sdata, model)) {
      LOG(INFO) << "Could not compute model";
      return false;
    }

    // 3. Count inlier-support.
    model_estimator.CalculateResiduals(ref_samples, qry_samples,
                                       *model, &residuals);
    for (int i = 0; i < nsamples; i++) if (residuals[i] < eps_) support++;

    // 4. Is it a better model?
    if (best_support < support) {
      // 4.1 Local-Refinement? (LO-RANSAC)
      // TODO(vfragoso): Implement a LO-RANSAC?

      // 4.2 Maximality condition:
      // Checks whether a solution was found within a certain amount of
      // iterations. If more than the expected then it is not a good one!
      inlier_ratio =
          static_cast<double>(support) / static_cast<double>(nsamples);
      const double Pin = pow(inlier_ratio, min_samples);
      const double k = log_eta / log(1.0 - Pin);

      // 4.3 Saving state iff maximality is satisfied.
      if (k > iter) {
        bestModel = *model;
        best_support = support;

        // 4.4 If number of iterations is adaptive then recompute
        // max_iterations.
        if (adaptive_) {
          const double w_n = pow(inlier_ratio, min_samples);
          max_iterations = static_cast<int>(log(1.0 - p_) / log(1.0 - w_n));
        }

        if (inlier_ratio >= alpha_) {
          LOG(INFO) << "Found model: iteration=" << iter
                    << " inlier_ratio=" << inlier_ratio;
          break;
        }
      }
    }
  }

  // Refine model with all the found inliers.
  model_estimator.CalculateResiduals(ref_samples, qry_samples,
                                     bestModel, &residuals);
  sdata.clear();
  for (int i = 0; i < nsamples; ++i) {
    (*inliers)[i] = residuals[i] <= eps_;
    if ((*inliers)[i]) sdata.push_back(i);
  }

  // Use the inliers to compute a new model.
  model_estimator.Compute(ref_samples, qry_samples, sdata, model);

  // Refine model using LM.
  return model_estimator.Refine(ref_samples, qry_samples, sdata, model);
}
#endif  // RANSAC_H_
