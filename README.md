Copyright (C) 2014 Victor Fragoso <vfragoso@cs.ucsb.edu>
--------------------------------------------------------------------------

EVSAC
-----

This example program shows how to use EVSAC to compute homography from
feature matches using OpenCV. We also include matlab scripts to 
compute the weights for every correspondence using EVSAC algorithm.

Dependencies
------------
1. OpenCV

2. STATX library: https://bitbucket.org/vfragoso/statx/

3. Optimo library: https://bitbucket.org/vfragoso/optimo/

4. CMake

Build
-----
To build and run the program, just follow these steps:

1. cmake .

2. make

3. bin/evsac_example -logtostderr=1 -ref_img=/path/to/img1.png -qry_img=/path/to/img2.png -nkpts=2000 -descriptor=SIFT

The images passed to the program have be compatible with OpenCV so that we can process them. 